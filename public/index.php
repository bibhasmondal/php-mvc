<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

header('Content-Type: application/json');
/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('home', ['controller' => 'Home', 'action' => 'index', 'method'=>'get']);
$router->add('home', ['controller' => 'Home', 'action' => 'test','method'=>'post']);
// $router->add('{controller}/{action}');
$router->dispatch($_SERVER['QUERY_STRING']);
