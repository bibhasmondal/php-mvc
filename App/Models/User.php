<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class User extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT id, name FROM users');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert($name,$surname, $sex){
        $sql = "INSERT INTO users (name, surname, sex) VALUES (?,?,?)";
        $db = static::getDB();
        $stmt= $db->prepare($sql);
        $stmt->execute([$name, $surname, $sex]);
    }
}
